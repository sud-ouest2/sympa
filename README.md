# Configuration et scripts pour le/les serveurs de listes de diffusions

L'infrastructure de sud-ouest2.org est prévue pour avoir plusieurs serveurx MX entrant et sortants ainsi qu'un ou plusieurs serveurs de listes de diffusions (utilisant SYMPA) et dont les mails sont routés par les serveurs mx-in et mx-out, vous trouverez donc dans ce dépot les fichiers de configuration et / ou les scripts utilisés pour ça

Le principe est le suivant:
* La gestion administrative est gérée par Dolibarr
* Le système central est géré par Modoboa
* Un ou plusieurs serveurs MX frontaux

## Installation OS

* OS de base : Debian GNU/Linux
* Paquets logiciels installés : voir le contenu du fichier dpkg.get_selections

## Configuration postfix
* Voir les fichiers présents dans le dépot


## Configuration sympa
* Voir les fichiers présents dans le dépot

## Scripts personnalisés

Tous les scripts personnalisés sont dans /usr/local/bin :

* soo2-update_sympa.sh: création automatique des robots virtuels pour les listes que nous hébergeons

un adhérent se connecte sur l'interface de gestion administrative (dolibarr) et dédide de nous confier la gestion du sous domaine listes.sondomaineperso.extension et quelques minutes plus tard tout est prêt sur ce serveur ... (en fonction du cron que vous avez configurés)


