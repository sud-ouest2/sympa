#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
#requete sur dolibarr pour avoir la liste des listes de diffusions ... https://sud-ouest2.org/dolibarr/public/soo/get_liste_sympa.php
SYMPAALIASDIR=/etc/mail/sympa/
export LC_ALL=C

FIC=`tempfile`

LADATE=`date "+%Y%m%d %H:%M"`
echo "------------- ${LADATE}"

wget -q https://sud-ouest2.org/dolibarr/public/soo/get_liste_sympa.php -O ${FIC}
if [ $? != 0 ]; then
    exit
fi

#pour ne pas tout refaire si pas de nouveautes
diff ${FIC} /etc/mail/sympa/mydestination
if [ $? == 0 ]; then
    #aucune modif -> exit
    rm ${FIC}
    exit
fi

#si le fichier contiens de la m**** on exit direct
TESTERRHTML=`grep "<" ${FIC}`
if [ -n "${TESTERRHTML}" ]; then
    echo "erreur de fichier ${FIC} : HTML tags, exit"
    rm ${FIC}
    exit
fi

for domaine in `cat ${FIC} /etc/mail/sympa/mydestination.orig | sort -u | grep -v "^#"`
do
    if [ ! -d /etc/sympa/$domaine ]; then
	mkdir /etc/sympa/$domaine
    fi
    listmaster=`echo $domaine | sed s/"^listes."/""/ | sed s/"^"/"listmaster@"/`
    if [ ! -f /etc/sympa/$domaine/robot.conf ]; then
	cat /etc/sympa/sudouest/ROBOT_MAQUETTE | sed s/"DOMAINE"/"$domaine"/g | sed s/"LISTMASTER"/"$listmaster"/g > /etc/sympa/$domaine/robot.conf
    fi
    chown sympa:sympa -R /etc/sympa/$domaine

    if [ ! -d /var/lib/sympa/list_data/$domaine ]; then
	mkdir /var/lib/sympa/list_data/$domaine
    fi
    chown sympa:sympa -R /var/lib/sympa/list_data/$domaine


    #et on ajoute les trois entrees de base pour les domaines virtuels dans /etc/mail/sympa/aliases-domains
    cat /etc/mail/sympa/ALIASES_DOMAINS_MAQUETTE | sed s/"DOMAINE"/"$domaine"/g >> /etc/mail/sympa/aliases-domains.new
done

cp /etc/mail/sympa/aliases-domains.new /etc/mail/sympa/aliases-domains
rm /etc/mail/sympa/aliases-domains.new

cp ${FIC} /etc/mail/sympa/mydestination
rm ${FIC}

NEWALIASFILE=`sympa --make_alias_file 2>&1 | grep "aliases file " | sed s/".*aliases file \(.*\) was.*"/"\1"/`
if [ -f "${NEWALIASFILE}" ]; then
    LADATE=`date "+%Y%m%d"`
    mv ${SYMPAALIASDIR}/aliases ${SYMPAALIASDIR}/archives/${LADATE}-aliases
    mv ${NEWALIASFILE} ${SYMPAALIASDIR}/aliases
fi

#alias & hash
cd /etc/mail/sympa
for fic in aliases aliases-domains aliases-global mydestination virtual
do
    postmap hash:${fic}
    chmod 644 ${fic}
done

newaliases

chown sympa:sympa /etc/mail/sympa -R

service sympa restart
service apache2 restart
service postfix restart

